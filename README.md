![Anurag’s github stats](https://github-readme-stats.vercel.app/api?username=lieber68&show_icons=true&theme=merko)

### 说明 :

* 只是搬运和同步更新大佬脚本.

* 不负责维护脚本.

* 只测试自用脚本，其他大部分脚本未测试可用性.

## 免责声明：

* lieber68发布的Script项目中涉及的任何解锁和解密分析脚本仅用于资源共享和学习研究，不能保证其合法性，准确性，完整性和有效性，请根据情况自行判断.

* 间接使用脚本的任何用户，包括但不限于建立VPS或在某些行为违反国家/地区法律或相关法规的情况下进行传播, lieber68 对于由此引起的任何隐私泄漏或其他后果概不负责.

* 请勿将Script项目的任何内容用于商业或非法目的，否则后果自负.

* 如果任何单位或个人认为该项目的脚本可能涉嫌侵犯其权利，则应及时通知并提供身份证明，所有权证明，我们将在收到认证文件后删除相关脚本.

* lieber68对任何脚本问题概不负责，包括但不限于由任何脚本错误导致的任何损失或损害.

* 您必须在下载后的24小时内从计算机或手机中完全删除以上内容.

* 任何以任何方式查看此项目的人或直接或间接使用该Script项目的任何脚本的使用者都应仔细阅读此声明。lieber68保留随时更改或补充此免责声明的权利。一旦使用并复制了任何相关脚本或Script项目的规则，则视为您已接受此免责声明.

### 特别感谢,排名不分先后：
* [@Hackl0us](https://github.com/Hackl0us)

* [@NobyDa](https://github.com/NobyDa)

* [@ConnersHua](https://github.com/DivineEngine/Profiles/tree/master)

* [@chavyleung](https://github.com/chavyleung)

* [@lxk0301](https://gitee.com/lxk0301/jd_scripts)

* [@Tartarus2014](https://github.com/Tartarus2014)

* [@blackmatrix7](https://github.com/blackmatrix7)

* [@Orz-3](https://github.com/Orz-3?tab=repositories)

* [@Peng-YM](https://github.com/Peng-YM?tab=repositories)

* [@zZPiglet](https://github.com/zZPiglet?tab=repositories)

* [@KOP-XIAO](https://github.com/KOP-XIAO?tab=repositories)

* [@Choler](https://github.com/Choler?tab=repositories)

* [@Koolson](https://github.com/Koolson?tab=repositories)

* [@HotKids](https://github.com/HotKids?tab=repositories)

* [@58xinian](https://github.com/58xinian?tab=repositories)

* [@yichahucha](https://github.com/yichahucha/surge/tree/master)

* [@ACL4SSR](https://github.com/ACL4SSR/ACL4SSR/tree/master)

* [@Loyalsoldier](https://github.com/Loyalsoldier?tab=repositories)

* [@ZhiYi-N](https://github.com/ZhiYi-N?tab=repositories)

* [@JMVoid](https://github.com/JMVoid?tab=repositories)

* [@alecthw](https://github.com/alecthw?tab=repositories)

* [@JDHelloWorld](https://github.com/JDHelloWorld?tab=repositories)

* [@limbopro](https://github.com/limbopro?tab=repositories)

## License

[GPLv3](LICENSE)